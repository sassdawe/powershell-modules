﻿<#
.SYNOPSIS
    Proxy function begin block
.DESCRIPTION
    The begin block of a proxy function.
#>
[System.Diagnostics.DebuggerStepThrough()]
param(
    # The name of the command being proxied.
    [System.String]
    $CommandName,

    # The type of the command being proxied. Valid values include 'Cmdlet' or 'Function'.
    [System.Management.Automation.CommandTypes]
    $CommandType,

    # A script block that is used to convert from the proxy command parameters to the parameters of the command being proxied.
    [System.Management.Automation.ScriptBlock]
    $PreProcessScriptBlock = {},

    # A command that will be used as a source of data to pipe into the function being proxied.
    [System.Management.Automation.ScriptBlock]
    $PipeFromCommand = $null,

    # A command that will used to process objects that are returned from the function being proxied.
    [System.Management.Automation.ScriptBlock]
    $PipeToCommand = $null
)
try {
    #region Ensure that objects are sent through the pipeline one at a time.

    $outBuffer = $null
    if ($PSCmdlet.MyInvocation.BoundParameters.TryGetValue('OutBuffer', [ref]$outBuffer)) {
        $PSCmdlet.MyInvocation.BoundParameters['OutBuffer'] = 1
    }

    #endregion

    #region Add empty credential support, regardless of the function being proxied.

    if ($PSCmdlet.MyInvocation.BoundParameters.ContainsKey('Credential') -and ($Credential -eq [System.Management.Automation.PSCredential]::Empty)) {
        $PSCmdlet.MyInvocation.BoundParameters.Remove('Credential') > $null
    }

    #endregion

    #region Look up the command being proxied.

    $wrappedCmd = $ExecutionContext.InvokeCommand.GetCommand($CommandName, $CommandType)

    #endregion

    #region If the command was not found, throw an appropriate command not found exception.

    if (-not $wrappedCmd) {
        $message = $PSCmdlet.GetResourceString('DiscoveryExceptions','CommandNotFoundException') -f $CommandName
        $exception = New-Object -TypeName System.Management.Automation.CommandNotFoundException -ArgumentList $message
        $exception.CommandName = $CommandName
        $errorRecord = New-Object -TypeName System.Management.Automation.ErrorRecord -ArgumentList $exception,'DiscoveryExceptions',([System.Management.Automation.ErrorCategory]::ObjectNotFound),$PSCmdlet.MyInvocation.MyCommand.Name
        throw $errorRecord
    }

    #endregion

    . $PreProcessScriptBlock

    #region Create the proxy command script block.

    $PSPassThruParameters = $PSCmdlet.MyInvocation.BoundParameters
    $scriptCmd = {& $wrappedCmd @PSPassThruParameters}

    #endregion

    #region Modify the proxy command script block if we have a PipeFromCommand or PipeToCommand.

    if ($PipeFromCommand) {
        $scriptCmd = [System.Management.Automation.ScriptBlock]::Create("${PipeFromCommand} | ${scriptCmd}")
    }
    if ($PipeToCommand) {
        $scriptCmd = [System.Management.Automation.ScriptBlock]::Create("${scriptCmd} | ${PipeToCommand}")
    }

    #endregion

    #region Use the script block to create the pipeline, then invoke its begin block.

    $pipeline = $scriptCmd.GetSteppablePipeline($myInvocation.CommandOrigin)
    $pipeline.Begin($PSCmdlet)

    #endregion
} catch {
    $PSCmdlet.ThrowTerminatingError($_)
}
# SIG # Begin signature block
# MIIX0AYJKoZIhvcNAQcCoIIXwTCCF70CAQExCzAJBgUrDgMCGgUAMGkGCisGAQQB
# gjcCAQSgWzBZMDQGCisGAQQBgjcCAR4wJgIDAQAABBAfzDtgWUsITrck0sYpfvNR
# AgEAAgEAAgEAAgEAAgEAMCEwCQYFKw4DAhoFAAQU2VMUaP3WsPL39EMoGbSMj/Rm
# aa6gghMDMIID7jCCA1egAwIBAgIQfpPr+3zGTlnqS5p31Ab8OzANBgkqhkiG9w0B
# AQUFADCBizELMAkGA1UEBhMCWkExFTATBgNVBAgTDFdlc3Rlcm4gQ2FwZTEUMBIG
# A1UEBxMLRHVyYmFudmlsbGUxDzANBgNVBAoTBlRoYXd0ZTEdMBsGA1UECxMUVGhh
# d3RlIENlcnRpZmljYXRpb24xHzAdBgNVBAMTFlRoYXd0ZSBUaW1lc3RhbXBpbmcg
# Q0EwHhcNMTIxMjIxMDAwMDAwWhcNMjAxMjMwMjM1OTU5WjBeMQswCQYDVQQGEwJV
# UzEdMBsGA1UEChMUU3ltYW50ZWMgQ29ycG9yYXRpb24xMDAuBgNVBAMTJ1N5bWFu
# dGVjIFRpbWUgU3RhbXBpbmcgU2VydmljZXMgQ0EgLSBHMjCCASIwDQYJKoZIhvcN
# AQEBBQADggEPADCCAQoCggEBALGss0lUS5ccEgrYJXmRIlcqb9y4JsRDc2vCvy5Q
# WvsUwnaOQwElQ7Sh4kX06Ld7w3TMIte0lAAC903tv7S3RCRrzV9FO9FEzkMScxeC
# i2m0K8uZHqxyGyZNcR+xMd37UWECU6aq9UksBXhFpS+JzueZ5/6M4lc/PcaS3Er4
# ezPkeQr78HWIQZz/xQNRmarXbJ+TaYdlKYOFwmAUxMjJOxTawIHwHw103pIiq8r3
# +3R8J+b3Sht/p8OeLa6K6qbmqicWfWH3mHERvOJQoUvlXfrlDqcsn6plINPYlujI
# fKVOSET/GeJEB5IL12iEgF1qeGRFzWBGflTBE3zFefHJwXECAwEAAaOB+jCB9zAd
# BgNVHQ4EFgQUX5r1blzMzHSa1N197z/b7EyALt0wMgYIKwYBBQUHAQEEJjAkMCIG
# CCsGAQUFBzABhhZodHRwOi8vb2NzcC50aGF3dGUuY29tMBIGA1UdEwEB/wQIMAYB
# Af8CAQAwPwYDVR0fBDgwNjA0oDKgMIYuaHR0cDovL2NybC50aGF3dGUuY29tL1Ro
# YXd0ZVRpbWVzdGFtcGluZ0NBLmNybDATBgNVHSUEDDAKBggrBgEFBQcDCDAOBgNV
# HQ8BAf8EBAMCAQYwKAYDVR0RBCEwH6QdMBsxGTAXBgNVBAMTEFRpbWVTdGFtcC0y
# MDQ4LTEwDQYJKoZIhvcNAQEFBQADgYEAAwmbj3nvf1kwqu9otfrjCR27T4IGXTdf
# plKfFo3qHJIJRG71betYfDDo+WmNI3MLEm9Hqa45EfgqsZuwGsOO61mWAK3ODE2y
# 0DGmCFwqevzieh1XTKhlGOl5QGIllm7HxzdqgyEIjkHq3dlXPx13SYcqFgZepjhq
# IhKjURmDfrYwggSjMIIDi6ADAgECAhAOz/Q4yP6/NW4E2GqYGxpQMA0GCSqGSIb3
# DQEBBQUAMF4xCzAJBgNVBAYTAlVTMR0wGwYDVQQKExRTeW1hbnRlYyBDb3Jwb3Jh
# dGlvbjEwMC4GA1UEAxMnU3ltYW50ZWMgVGltZSBTdGFtcGluZyBTZXJ2aWNlcyBD
# QSAtIEcyMB4XDTEyMTAxODAwMDAwMFoXDTIwMTIyOTIzNTk1OVowYjELMAkGA1UE
# BhMCVVMxHTAbBgNVBAoTFFN5bWFudGVjIENvcnBvcmF0aW9uMTQwMgYDVQQDEytT
# eW1hbnRlYyBUaW1lIFN0YW1waW5nIFNlcnZpY2VzIFNpZ25lciAtIEc0MIIBIjAN
# BgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAomMLOUS4uyOnREm7Dv+h8GEKU5Ow
# mNutLA9KxW7/hjxTVQ8VzgQ/K/2plpbZvmF5C1vJTIZ25eBDSyKV7sIrQ8Gf2Gi0
# jkBP7oU4uRHFI/JkWPAVMm9OV6GuiKQC1yoezUvh3WPVF4kyW7BemVqonShQDhfu
# ltthO0VRHc8SVguSR/yrrvZmPUescHLnkudfzRC5xINklBm9JYDh6NIipdC6Anqh
# d5NbZcPuF3S8QYYq3AhMjJKMkS2ed0QfaNaodHfbDlsyi1aLM73ZY8hJnTrFxeoz
# C9Lxoxv0i77Zs1eLO94Ep3oisiSuLsdwxb5OgyYI+wu9qU+ZCOEQKHKqzQIDAQAB
# o4IBVzCCAVMwDAYDVR0TAQH/BAIwADAWBgNVHSUBAf8EDDAKBggrBgEFBQcDCDAO
# BgNVHQ8BAf8EBAMCB4AwcwYIKwYBBQUHAQEEZzBlMCoGCCsGAQUFBzABhh5odHRw
# Oi8vdHMtb2NzcC53cy5zeW1hbnRlYy5jb20wNwYIKwYBBQUHMAKGK2h0dHA6Ly90
# cy1haWEud3Muc3ltYW50ZWMuY29tL3Rzcy1jYS1nMi5jZXIwPAYDVR0fBDUwMzAx
# oC+gLYYraHR0cDovL3RzLWNybC53cy5zeW1hbnRlYy5jb20vdHNzLWNhLWcyLmNy
# bDAoBgNVHREEITAfpB0wGzEZMBcGA1UEAxMQVGltZVN0YW1wLTIwNDgtMjAdBgNV
# HQ4EFgQURsZpow5KFB7VTNpSYxc/Xja8DeYwHwYDVR0jBBgwFoAUX5r1blzMzHSa
# 1N197z/b7EyALt0wDQYJKoZIhvcNAQEFBQADggEBAHg7tJEqAEzwj2IwN3ijhCcH
# bxiy3iXcoNSUA6qGTiWfmkADHN3O43nLIWgG2rYytG2/9CwmYzPkSWRtDebDZw73
# BaQ1bHyJFsbpst+y6d0gxnEPzZV03LZc3r03H0N45ni1zSgEIKOq8UvEiCmRDoDR
# EfzdXHZuT14ORUZBbg2w6jiasTraCXEQ/Bx5tIB7rGn0/Zy2DBYr8X9bCT2bW+IW
# yhOBbQAuOA2oKY8s4bL0WqkBrxWcLC9JG9siu8P+eJRRw4axgohd8D20UaF5Mysu
# e7ncIAkTcetqGVvP6KUwVyyJST+5z3/Jvz4iaGNTmr1pdKzFHTx/kuDDvBzYBHUw
# ggUwMIIEGKADAgECAhAECRgbX9W7ZnVTQ7VvlVAIMA0GCSqGSIb3DQEBCwUAMGUx
# CzAJBgNVBAYTAlVTMRUwEwYDVQQKEwxEaWdpQ2VydCBJbmMxGTAXBgNVBAsTEHd3
# dy5kaWdpY2VydC5jb20xJDAiBgNVBAMTG0RpZ2lDZXJ0IEFzc3VyZWQgSUQgUm9v
# dCBDQTAeFw0xMzEwMjIxMjAwMDBaFw0yODEwMjIxMjAwMDBaMHIxCzAJBgNVBAYT
# AlVTMRUwEwYDVQQKEwxEaWdpQ2VydCBJbmMxGTAXBgNVBAsTEHd3dy5kaWdpY2Vy
# dC5jb20xMTAvBgNVBAMTKERpZ2lDZXJ0IFNIQTIgQXNzdXJlZCBJRCBDb2RlIFNp
# Z25pbmcgQ0EwggEiMA0GCSqGSIb3DQEBAQUAA4IBDwAwggEKAoIBAQD407Mcfw4R
# r2d3B9MLMUkZz9D7RZmxOttE9X/lqJ3bMtdx6nadBS63j/qSQ8Cl+YnUNxnXtqrw
# nIal2CWsDnkoOn7p0WfTxvspJ8fTeyOU5JEjlpB3gvmhhCNmElQzUHSxKCa7JGnC
# wlLyFGeKiUXULaGj6YgsIJWuHEqHCN8M9eJNYBi+qsSyrnAxZjNxPqxwoqvOf+l8
# y5Kh5TsxHM/q8grkV7tKtel05iv+bMt+dDk2DZDv5LVOpKnqagqrhPOsZ061xPeM
# 0SAlI+sIZD5SlsHyDxL0xY4PwaLoLFH3c7y9hbFig3NBggfkOItqcyDQD2RzPJ6f
# pjOp/RnfJZPRAgMBAAGjggHNMIIByTASBgNVHRMBAf8ECDAGAQH/AgEAMA4GA1Ud
# DwEB/wQEAwIBhjATBgNVHSUEDDAKBggrBgEFBQcDAzB5BggrBgEFBQcBAQRtMGsw
# JAYIKwYBBQUHMAGGGGh0dHA6Ly9vY3NwLmRpZ2ljZXJ0LmNvbTBDBggrBgEFBQcw
# AoY3aHR0cDovL2NhY2VydHMuZGlnaWNlcnQuY29tL0RpZ2lDZXJ0QXNzdXJlZElE
# Um9vdENBLmNydDCBgQYDVR0fBHoweDA6oDigNoY0aHR0cDovL2NybDQuZGlnaWNl
# cnQuY29tL0RpZ2lDZXJ0QXNzdXJlZElEUm9vdENBLmNybDA6oDigNoY0aHR0cDov
# L2NybDMuZGlnaWNlcnQuY29tL0RpZ2lDZXJ0QXNzdXJlZElEUm9vdENBLmNybDBP
# BgNVHSAESDBGMDgGCmCGSAGG/WwAAgQwKjAoBggrBgEFBQcCARYcaHR0cHM6Ly93
# d3cuZGlnaWNlcnQuY29tL0NQUzAKBghghkgBhv1sAzAdBgNVHQ4EFgQUWsS5eyoK
# o6XqcQPAYPkt9mV1DlgwHwYDVR0jBBgwFoAUReuir/SSy4IxLVGLp6chnfNtyA8w
# DQYJKoZIhvcNAQELBQADggEBAD7sDVoks/Mi0RXILHwlKXaoHV0cLToaxO8wYdd+
# C2D9wz0PxK+L/e8q3yBVN7Dh9tGSdQ9RtG6ljlriXiSBThCk7j9xjmMOE0ut119E
# efM2FAaK95xGTlz/kLEbBw6RFfu6r7VRwo0kriTGxycqoSkoGjpxKAI8LpGjwCUR
# 4pwUR6F6aGivm6dcIFzZcbEMj7uo+MUSaJ/PQMtARKUT8OZkDCUIQjKyNookAv4v
# cn4c10lFluhZHen6dGRrsutmQ9qzsIzV6Q3d9gEgzpkxYz0IGhizgZtPxpMQBvwH
# gfqL2vmCSfdibqFT+hKUGIUukpHqaGxEMrJmoecYpJpkUe8wggUyMIIEGqADAgEC
# AhAF6V5I5jKh5PIOTECyjmcvMA0GCSqGSIb3DQEBCwUAMHIxCzAJBgNVBAYTAlVT
# MRUwEwYDVQQKEwxEaWdpQ2VydCBJbmMxGTAXBgNVBAsTEHd3dy5kaWdpY2VydC5j
# b20xMTAvBgNVBAMTKERpZ2lDZXJ0IFNIQTIgQXNzdXJlZCBJRCBDb2RlIFNpZ25p
# bmcgQ0EwHhcNMTgwNDEwMDAwMDAwWhcNMTgxMjE5MTIwMDAwWjBvMQswCQYDVQQG
# EwJDQTEWMBQGA1UECBMNTmV3IEJydW5zd2ljazEQMA4GA1UEBxMHU2hlZGlhYzEa
# MBgGA1UEChMRS2lyayBBbmRyZXcgTXVucm8xGjAYBgNVBAMTEUtpcmsgQW5kcmV3
# IE11bnJvMIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEA+LFqxPDdvIdO
# MtmTzZEUA0KVdwFo1s23OAuqmOQweomPauxEVegB+x9+FoFG6cAZVoZ9Q+eW/Avg
# Nc44S2WQPKIFKhV8EJYdWgB/n6z3I6yW5LPlGjyNB6Zbl85CA6qKq+3HVpPl6Wjw
# Pw94/1VcP9Eqx7KQq1ZHWmPmjCMe1wtrbaI+W7SO4pdZh25Wy5kh6BHN6KRlh6VA
# T718STxEDu0pMJdfB6i+uPvntbEt/158yc1da3ZmE8Neej5p7QcWWOiM3TC6hbCQ
# TZfl0ybyAIPB9sbUqG8SFzMYyAb8zSiUC5JeQcM5qkmmqm4ymW0d8YHhQcPX+dhm
# SUbrRS+98QIDAQABo4IBxTCCAcEwHwYDVR0jBBgwFoAUWsS5eyoKo6XqcQPAYPkt
# 9mV1DlgwHQYDVR0OBBYEFCDLJR7k6dkAmWoQu4UhyNZjy7g0MA4GA1UdDwEB/wQE
# AwIHgDATBgNVHSUEDDAKBggrBgEFBQcDAzB3BgNVHR8EcDBuMDWgM6Axhi9odHRw
# Oi8vY3JsMy5kaWdpY2VydC5jb20vc2hhMi1hc3N1cmVkLWNzLWcxLmNybDA1oDOg
# MYYvaHR0cDovL2NybDQuZGlnaWNlcnQuY29tL3NoYTItYXNzdXJlZC1jcy1nMS5j
# cmwwTAYDVR0gBEUwQzA3BglghkgBhv1sAwEwKjAoBggrBgEFBQcCARYcaHR0cHM6
# Ly93d3cuZGlnaWNlcnQuY29tL0NQUzAIBgZngQwBBAEwgYQGCCsGAQUFBwEBBHgw
# djAkBggrBgEFBQcwAYYYaHR0cDovL29jc3AuZGlnaWNlcnQuY29tME4GCCsGAQUF
# BzAChkJodHRwOi8vY2FjZXJ0cy5kaWdpY2VydC5jb20vRGlnaUNlcnRTSEEyQXNz
# dXJlZElEQ29kZVNpZ25pbmdDQS5jcnQwDAYDVR0TAQH/BAIwADANBgkqhkiG9w0B
# AQsFAAOCAQEASD7vvOBljJJuQTwD0I+J8ZouxDK8ORJ/2JUUslEFIIZRobfInmzV
# d4ESbqn3vl8XLiBV3l4eFPm7B2Pqinm8zC6bbCig1ZwO5D42vDfVuQ5Go0tG5obj
# jCmmnCNp+jhzEJrJS9Ioz3qeAzqsMTPzMQKsevOfIUoFjeWJt2MkfpcswdvLv4oD
# VEVyLHYA52fDm6YdKxKfDlaizRPIU6jbkV9lO8SuSxpjgepPlRpNRRwDxC2R56Og
# UB7Ob1wcUSMfm67mCougUumXL8bUbMdWgCZYhJT2AxtfeL6RImULxqkEarZJXmpe
# 1PgQWT3+V2cEgjejgjHZDT3q0sxOeeZzyTGCBDcwggQzAgEBMIGGMHIxCzAJBgNV
# BAYTAlVTMRUwEwYDVQQKEwxEaWdpQ2VydCBJbmMxGTAXBgNVBAsTEHd3dy5kaWdp
# Y2VydC5jb20xMTAvBgNVBAMTKERpZ2lDZXJ0IFNIQTIgQXNzdXJlZCBJRCBDb2Rl
# IFNpZ25pbmcgQ0ECEAXpXkjmMqHk8g5MQLKOZy8wCQYFKw4DAhoFAKB4MBgGCisG
# AQQBgjcCAQwxCjAIoAKAAKECgAAwGQYJKoZIhvcNAQkDMQwGCisGAQQBgjcCAQQw
# HAYKKwYBBAGCNwIBCzEOMAwGCisGAQQBgjcCARUwIwYJKoZIhvcNAQkEMRYEFKfP
# wkMz4auDvBngT40oQJQ8MsA8MA0GCSqGSIb3DQEBAQUABIIBAGaPf8L4zuIqIR/7
# IkqWjGcNl4wBDE5z/MisU9tEBVUMC0qFxq28XMuHM+EIYEYTslc/QbP0tpXQvaQI
# vG/WZpt64rR9PsUvu96ZuISADign0nsEG2e1WnCCddH8PBTOuwHmrrW1ouG3fe5w
# zgLy9W0wfWhl84/VN46EQKiNC3AiyE6zIeTPGEiOtbrKnr6+ONPoWePdI4dz68sH
# oyAJnc7BoX1x8gIdYQOgPDnFvxK+1RjIAQ/YtSa/mbCEgJx38chjWmJJhAjrk+Uy
# /hc9Qz6Xw/Kc2OgWZ96GXH+exnExnVhIMXqmNIVS8xyhnRt3xTD87Gyse5lDK31a
# jY9jYKChggILMIICBwYJKoZIhvcNAQkGMYIB+DCCAfQCAQEwcjBeMQswCQYDVQQG
# EwJVUzEdMBsGA1UEChMUU3ltYW50ZWMgQ29ycG9yYXRpb24xMDAuBgNVBAMTJ1N5
# bWFudGVjIFRpbWUgU3RhbXBpbmcgU2VydmljZXMgQ0EgLSBHMgIQDs/0OMj+vzVu
# BNhqmBsaUDAJBgUrDgMCGgUAoF0wGAYJKoZIhvcNAQkDMQsGCSqGSIb3DQEHATAc
# BgkqhkiG9w0BCQUxDxcNMTgwNDEwMTIxNTE5WjAjBgkqhkiG9w0BCQQxFgQUDpkT
# teO58iImjAVucTyzm8z48iUwDQYJKoZIhvcNAQEBBQAEggEAIrLTe/pewEDyqT8H
# RS92xAc+GpywZRYuwySEaezBZ2Q9L9jDYXfL2uG9Dv9eVKpn9ODsuIGsJp94Oxrk
# cJqouGWLsAGn/4G6zietKdKw/diN3uA1ZX2aT3SZw2E6KIXHxQvlC8p4WW4lujXn
# ybR5y/qKTkviQK4bJLIkrkoXPInhZC5E4Q6A/u2H8rt5hnBSFI0+rVM9cErk66Ec
# rp4c2nVk10xeeGj/B4fXLyHXJRvXiWncDKjJLVCcbbZ2LITFGLEYAzMqUqMnXsBq
# WEFxDjq+1zjatTwXZZtbL3YC+a+S0xopec4SqpiREsURnND3zEhrXKb51YjpmRWN
# 3812PA==
# SIG # End signature block
