# DefenderASR

## Microsoft Defender Attack Surface Reduction configurator

This modules was created to help manually configure the local ASR settings of a machine in a small scale scenario where doesn't make sence to use System Center Configuration Manager, Microsoft Intune or Group Policies to deploy the settings.
