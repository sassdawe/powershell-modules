$ProgressPreference = 'SilentlyContinue'
$PSDefaultParameterValues["Out-Default:OutVariable"] = "__"
#region from https://twitter.com/cl/status/1446446992633237508?s=20
$PSDefaultParameterValues["Install-Module:Scope"] = "CurrentUser"
$PSDefaultParameterValues["Install-Module:Repository"] = "PSGallery"
$PSDefaultParameterValues["Install-Package:SkipDependencies"] = $true
$PSDefaultParameterValues["Install-Package:Confirm"] = $false
#endregion

$hosts = "C:\Windows\System32\drivers\etc\hosts"
$InstallPath = "C:\Source\powershell-modules\Modules"

$PSDefaultParameterValues["Save-Module:Path"] = $InstallPath

function edit-hosts {
    Show-PSEditor $hosts
}

$Global:PoshSessionID = [System.Guid]::NewGuid().Guid.ToString()
$Global:hostname = $(hostname)
$Global:whoami = $(whoami)
$Global:LastHistoryId = 0
function Get-ElevationStatus {
    [CmdletBinding()]
    [Alias("IsElevated")]
    param (
    )
    begin {
    }
    process {
        if ((New-Object Security.Principal.WindowsPrincipal([Security.Principal.WindowsIdentity]::GetCurrent())).IsInRole([Security.Principal.WindowsBuiltInRole]::Administrator)) {
            Write-Output "Elevated."
        }
        else {
            Write-Output "Not elevated."
        }
    }
    end {
    }
}
$Global:IsElevated = Get-ElevationStatus

Function Debug-PowerShell() {
    [CmdletBinding()]
    [Alias("dbps")]
    param (
        [string]$Path = "$ENV:TEMP\psdebu.log"
    )
    $Global:DebugLog = $Path
    New-Item $Global:DebugLog -Force -ItemType File
    Start-Process powershell -ArgumentList "-noprofile", "-command &{Get-Content '$DebugLog' -Wait}"
}

#region Application Insights
try {
    if ( [System.String]::IsNullOrEmpty($env:InstrumentationKey) ) {
        throw "InstrumentationKey is not set"
    }
    else {
        [Reflection.Assembly]::LoadFile("$([environment]::getfolderpath("mydocuments"))\WindowsPowerShell\Microsoft.ApplicationInsights\net46\Microsoft.ApplicationInsights.dll") | Out-Null
        $Global:TelClient = New-Object "Microsoft.ApplicationInsights.TelemetryClient"
        $Global:TelClient.InstrumentationKey = $env:InstrumentationKey
        $Global:TelClient.Context.Session.Id = $Global:PoshSessionID
        $Global:TelClient.Context.User.Id = $Global:whoami
        $Global:TelClient.Context.Device.Id = $Global:hostname
        #FIXME: $Global:TelClient.context.Properties.Add("isElevated", $Global:IsElevated)
        if ($TelClient) {
            #$dict = New-Object 'system.collections.generic.dictionary[[string],[object]]'
            #$dict.Add("hostname",$(hostname))
            #$dict.Add("whoami",$(whoami))
            #$dict.Add("isElevated",$Global:IsElevated)
            #$dict.Add("PoshSessionID",$Global:PoshSessionID)
            #$metric = New-Object 'system.collections.generic.dictionary[[string],[double]]'
            #$metric.Add("Pid",$pid.ToDouble([cultureinfo]::CurrentCulture))

            $EventTelemetry = New-Object "Microsoft.ApplicationInsights.DataContracts.EventTelemetry"
            $EventTelemetry.Name = "Windows PowerShell started"
            #$EventTelemetry.Properties["hostname"] = $Global:hostname
            #$EventTelemetry.Properties["whoami"] = $Global:whoami
            #$EventTelemetry.Properties["isElevated"] = $Global:IsElevated
            #$EventTelemetry.Properties["PoshSessionID"] = $Global:PoshSessionID

            $TelClient.TrackEvent($EventTelemetry)
            $TelClient.Flush()
        }
    }
}
catch { $_ }
#endregion

#Import-Module "$( ($profile | split-path))\Modules\Pester\*\Pester.psd1"


$env:PSModulePath += ";$InstallPath"

$env:psmodulepath = (($env:psmodulepath.Split(';') | Where-Object { -not [system.string]::IsNullOrEmpty($_) } | Sort-Object -Unique ) + $myModulePath) -join ';'

$env:PSModulePath += ";C:\Source\_self"

Import-Module Configuration -ErrorAction SilentlyContinue
Import-Module Terminal-Icons -ErrorAction SilentlyContinue

[System.Net.WebRequest]::DefaultWebProxy.Credentials = [System.Net.CredentialCache]::DefaultCredentials
# [Net.ServicePointManager]::SecurityProtocol = [Net.SecurityProtocolType]::Tls12

New-Alias ip Invoke-Pester -Force

function Copy-Object {
    param (
        [PSCustomObject]
        $Object
    )
    $Object.PSObject.Copy()
}

function Copy-History() { (Get-History).commandline | clip }
New-Alias -Name ch -Value 'Copy-History'

#Write-Host "Hey Master Skills!" -ForegroundColor Magenta

function clear-url {
    [cmdletbinding()]
    [alias('cc')]
    param(
        [string]$url = @(Get-Clipboard)[0]
    )
    $url = $url.Trim()
    Write-Verbose "original url: `'$url`'"
    if ( ([uri]$url).Query ) {
        Write-Verbose "removing: `'$(([uri]$url).Query)`'"
        ([uri]$url).AbsoluteUri.replace(([uri]$url).Query, '').trim() | clip
    }
    elseif ( ([uri]$url).Fragment ) {
        Write-Verbose "removing: `'$(([uri]$url).Fragment)`'"
        ([uri]$url).AbsoluteUri.replace(([uri]$url).Fragment, '').trim() | clip
    }
    Write-Verbose "clipboard updated with `'$((Get-Clipboard)[0])`'"
}

function Get-IPConfig {
    [CmdletBinding()]
    param (
        [switch]
        $All
    )
    $info = Get-NetIPAddress | Where-Object { $_.SuffixOrigin -ne "Link" } | Sort-Object ifIndex | ForEach-Object { 
        $ip = $_ 
        Get-NetAdapter -ifIndex $ip.ifIndex -ErrorAction SilentlyContinue | ForEach-Object {
            $out = @{ };
            $out.Name = $_.Name;
            $out.ifIndex = $_.ifIndex;
            $out.IPAddress = $ip.IPAddress;
            $out.PrefixLength = $ip.PrefixLength;
            $out.MacAddress = $_.MacAddress;
            $out.InterfaceDescription = $_.InterfaceDescription;
            $out.Status = $_.Status;
            [pscustomobject]$out
        }
    } 
    if ( $All ) {
        $info #| Format-Table -AutoSize
    }
    else {
        $info | Where-Object { -not $_.InterfaceDescription.startsWith("Hyper") }
    }
    
}

<# function Prompt {
    $host.ui.RawUI.WindowTitle = "$IsElevated - $(Get-Location)"
    Write-Host "PS " -NoNewLine
    Write-Host $([char]9829) -ForegroundColor Red -NoNewLine
    " > "
} #>

function Prompt {
    # borrowing heavily from https://dbatools.io/prompt but formatting the execution time without using the DbaTimeSpanPretty C# type
    Write-Host (Get-Date -Format "ddd HH:mm") -ForegroundColor Magenta -NoNewline
    try {
        $history = Get-History -ErrorAction Ignore -Count 1
        if ($history) {
            $ts = New-TimeSpan $history.StartExecutionTime $history.EndExecutionTime
            if ( $Global:LastHistoryId -ne $history.Id ) {
                $EventTelemetry = New-Object "Microsoft.ApplicationInsights.DataContracts.EventTelemetry"
                $EventTelemetry.Name = $history.CommandLine
                $EventTelemetry.Properties["hostname"] = $Global:hostname
                $EventTelemetry.Properties["whoami"] = $Global:whoami
                $EventTelemetry.Properties["isElevated"] = $Global:IsElevated
                $EventTelemetry.Properties["PoshSessionID"] = $Global:PoshSessionID
                $EventTelemetry.Properties["StartExecutionTime"] = $history.StartExecutionTime
                $EventTelemetry.Properties["ExecutionStatus"] = $history.ExecutionStatus
                $EventTelemetry.Properties["EndExecutionTime"] = $history.EndExecutionTime
                $EventTelemetry.Properties["ElapsedTime"] = $ts.ToString()


                $TelClient.TrackEvent($EventTelemetry)
                $TelClient.Flush()
                $Global:LastHistoryId = $history.Id
            }
            switch ($ts) {
                { $_.totalminutes -gt 1 -and $_.totalminutes -lt 30 } {
                    Write-Host " [" -ForegroundColor Red -NoNewline
                    [decimal]$d = $_.TotalMinutes
                    '{0:f3}m' -f ($d) | Write-Host -ForegroundColor Red -NoNewline
                    Write-Host "]" -ForegroundColor Red -NoNewline
                }
                { $_.totalminutes -le 1 -and $_.TotalSeconds -gt 1 } {
                    Write-Host " [" -ForegroundColor Yellow -NoNewline
                    [decimal]$d = $_.TotalSeconds
                    '{0:f3}s' -f ($d) | Write-Host -ForegroundColor Yellow -NoNewline
                    Write-Host "]" -ForegroundColor Yellow -NoNewline
                }
                { $_.TotalSeconds -le 1 } {
                    [decimal]$d = $_.TotalMilliseconds
                    Write-Host " [" -ForegroundColor Green -NoNewline
                    '{0:f3}ms' -f ($d) | Write-Host -ForegroundColor Green -NoNewline
                    Write-Host "]" -ForegroundColor Green -NoNewline
                }
                Default {
                    $_.Milliseconds | Write-Host -ForegroundColor Gray -NoNewline
                }
            }
        }
    }
    catch { }
    Write-Host " $($pwd.path.Split('\')[-2..-1] -join '\') " -NoNewline
    Write-Host $([char]9829) -ForegroundColor Red -NoNewline
    " > "
}

New-Alias mc 'C:\Program Files (x86)\Midnight Commander\mc.exe' -Force

# Chocolatey profile
$ChocolateyProfile = "$env:ChocolateyInstall\helpers\chocolateyProfile.psm1"
if (Test-Path($ChocolateyProfile)) {
    Import-Module "$ChocolateyProfile"
}

Set-PSReadLineOption -AddToHistoryHandler {
    param([string]$line)

    $sensitive = "password|asplaintext|token|key|secret"
    return ($line -notmatch $sensitive)
}
Set-PSReadLineOption -PredictionSource History


function update-mymodules {
    Get-ChildItem $InstallPath | ForEach-Object { Save-Module $_.Name }
    Get-ChildItem $InstallPath | ForEach-Object { Get-ChildItem $_.fullname -Directory | Sort-Object LastWriteTime -Descending | Select-Object -Skip 1 | Remove-Item -Force -Recurse }
}


az config set extension.use_dynamic_install=yes_without_prompt #--allow-preview true